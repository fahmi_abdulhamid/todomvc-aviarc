/*global
YAHOO
*/

(function () {

    YAHOO.namespace("support.v1_0_0");
    var support = YAHOO.support.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    support.AddClass = function () {
        
    };

    YAHOO.lang.extend(support.AddClass, Toronto.framework.DefaultActionImpl, {

        run: function (state) {
        
            var widgetName = this.getAttribute("widget", state);
            var classNameAttribute = this.getAttribute("class", state);
            
            
            var classNames = classNameAttribute.split(" ");
            
            var theWidget = state.getExecutionState().getWidgetContext().findWidgetByID(widgetName);
            if (theWidget !== undefined){
                for (var i=0; i<classNames.length; i++) {
                    theWidget.removeClass(classNames[i]);
                    theWidget.addClass(classNames[i]);
                }
            }
        }

    });

})();
