/*global
YAHOO
*/

(function () {
    YAHOO.namespace("datasetUtil.v1_0_0");
    var datasetUtil = YAHOO.datasetUtil.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    datasetUtil.MarkRowDeletedWhere = function () {
    };

    YAHOO.lang.extend(datasetUtil.MarkRowDeletedWhere, Toronto.framework.DefaultActionImpl, {
        run: function (state) {
            var fieldSpecifier = this.getAttribute('field', state);
            var is = this.getAttribute('is', state);
            
            var fieldParts = this._splitFieldSpecifier(fieldSpecifier);
            var datasetName = fieldParts.datasetName;
            var fieldName = fieldParts.fieldName;
    
            var dataset = state.getApplicationState().getDatasetStack().findDataset(datasetName);
            dataset.getAllRows()
                .filter(function(row) {
                    return row.getField(fieldName) === is;
                })
                .forEach(function(row) {
                    row.markDeleted();
                });
        },
        
        _splitFieldSpecifier: function(fieldSpecifier) {
            var fieldParts = fieldSpecifier.split('.');
            if(fieldParts.length !== 2) {
                throw new Error('Expected field to be in "dataset.field" notation. Field: "' + fieldSpecifier + '".');
            }
            
            return {
                datasetName: fieldParts[0],
                fieldName: fieldParts[1]
            };
        }
    });
})();
