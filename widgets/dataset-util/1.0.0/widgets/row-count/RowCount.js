/*global
YAHOO
*/

(function() {
    YAHOO.namespace("datasetUtil.v1_0_0");
    var datasetUtil = YAHOO.datasetUtil.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    datasetUtil.RowCount = function() {

    };

    YAHOO.lang.extend(datasetUtil.RowCount, Toronto.framework.DefaultActionImpl, {
        run: function(state) {
            var datasetName = this.getAttribute('dataset', state);

            var dataset = state.getApplicationState().getDatasetStack().findDataset(datasetName);
            var rowCount = dataset.getRowCount();
            
            state.getExecutionState().setReturnValue(rowCount);
        }
    });
})();