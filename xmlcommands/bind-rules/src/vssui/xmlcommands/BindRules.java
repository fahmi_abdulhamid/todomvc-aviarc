package vssui.xmlcommands;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.aviarc.core.command.Command;
import com.aviarc.core.dataset.Dataset;
import com.aviarc.core.exceptions.CommandException;
import com.aviarc.core.exceptions.NoSuchTemporaryFileException;
import com.aviarc.core.resource.ResourceDirectory;
import com.aviarc.core.resource.ResourceFile;
import com.aviarc.core.resource.TemporaryResourceID;
import com.aviarc.core.resource.TemporaryResourceManager;
import com.aviarc.core.runtimevalues.RuntimeValue;
import com.aviarc.core.state.State;
import com.aviarc.framework.datarule.DataRuleUtil;
import com.aviarc.framework.datarule.DataRuleUtil.DataRuleException;
import com.aviarc.framework.xml.command.AbstractXMLCommand;
import com.aviarc.framework.xml.compilation.CompiledElementContext;

public class BindRules extends AbstractXMLCommand {

	private static final long serialVersionUID = 0L;

	private BindSet _bindSet;
	private RuntimeValue<String> _dataset;
	private RuntimeValue<String> _rulesFile;

    private RuntimeValue<String> _rulesetKey = null;

	public void doInitialize(InitializationContext ctx) {
		_bindSet = new BindSet();
		_dataset = ctx.getElementContext().getAttribute("dataset").getRuntimeValue();
		
		
		if (ctx.getElementContext().getAttribute("rules-file") != null) {
			_rulesFile = ctx.getElementContext().getAttribute("rules-file").getRuntimeValue();
		}
		if (ctx.getElementContext().getAttribute("ruleset-key") != null) {
		    _rulesetKey  = ctx.getElementContext().getAttribute("ruleset-key").getRuntimeValue();
        }
		for (CompiledElementContext<Command> rule : ctx.getElementContext().getSubElements("rule")) {
			BindRule br = new BindRule(rule.getAttribute("name").getRuntimeValue(), rule.getAttribute("namespace").getRuntimeValue());
			for (CompiledElementContext<Command> attribute : rule.getSubElements("attribute")) {
				RuntimeValue<String> name = attribute.getAttribute("name").getRuntimeValue();
				RuntimeValue<String> value = attribute.getAttribute("value").getRuntimeValue();
				BindAttribute att = new BindAttribute(name, value);
				br.getAttributes().add(att);
			}
			_bindSet.addRule(br);
		}
	}

	// DataRuleUtil.bindDataRules(dataset, currentState, someParameters,
	// rulesFile);

	public void run(State state) {
		Dataset dataset = state.getApplicationState().getDatasetStack().findDataset(_dataset.getValue(state));
		Visitor visitor = new VisitorImpl(state);
		_bindSet.accept(visitor);
		String xml = visitor.toString();
		TemporaryResourceManager temporaryResourceManager = state.getCurrentApplicationInstance()
		        .getTemporaryResourceManager();
		TemporaryResourceID resourceID = temporaryResourceManager.createNewTemporaryFile();
		ResourceFile file = null;
		try {
			file = temporaryResourceManager.retrieveTemporaryFile(resourceID);
			OutputStream outputStream = file.getOutputStream();
			outputStream.write(xml.getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (NoSuchTemporaryFileException e) {
			throw new CommandException(e);
		} catch (IOException e) {
			throw new CommandException(e);
		}
		Map<String, String> someParameters = new HashMap<String, String>();
		
		String key;
		if (_rulesetKey != null) {
		    key = _rulesetKey.getValue(state);
		} else {
		    key = "bind-rules-" + new Double(Math.random()).toString();
		}

		try {
			DataRuleUtil.bindDataRules(dataset, state, someParameters, file, key);
		} catch (DataRuleException e) {
		    throw new CommandException(e);
        }
		
		file.delete();
		
		if (_rulesFile != null) {
			ResourceDirectory customRulesDir = state.getCurrentApplicationInstance().getApplication()
			        .getMetadataDirectory().getDirectory("custom-rules");
			ResourceFile rulesFile = customRulesDir.getFile(String.format("%s.xml", _rulesFile.getValue(state)));
			try {
			    DataRuleUtil.bindDataRules(dataset, state, someParameters, rulesFile, "bind-rules");
			} catch (DataRuleException e) {
			    throw new CommandException(e);
            }
		}
		
		
	}

}
