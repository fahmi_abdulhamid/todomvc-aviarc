/*global
YAHOO
*/

(function () {
    YAHOO.namespace("datasetUtil.v1_0_0");
    var application = YAHOO.datasetUtil.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.AllFieldsEqual = function () {
        
    };

    YAHOO.lang.extend(application.AllFieldsEqual, Toronto.framework.DefaultActionImpl, {
        run: function (state) {
            var fieldSpecifier = this.getAttribute('field', state);
            var value = this.getAttribute('value', state);
            
            var fieldParts = this._splitFieldSpecifier(fieldSpecifier);
            var datasetName = fieldParts.datasetName;
            var fieldName = fieldParts.fieldName;
            
            var dataset = state.getApplicationState().getDatasetStack().findDataset(datasetName);
            
            var matchingRows = dataset.getAllRows()
                .filter(function(row) {
                    return row.getField(fieldName) === value;
                });
            
            var fieldsAreEqual = matchingRows.length === dataset.getRowCount();
            state.getExecutionState().setReturnValue(fieldsAreEqual);
        },
        
        _splitFieldSpecifier: function(fieldSpecifier) {
            var fieldParts = fieldSpecifier.split('.');
            if(fieldParts.length !== 2) {
                throw new Error('Expected field to be in "dataset.field" notation. Field: "' + fieldSpecifier + '".');
            }
            
            return {
                datasetName: fieldParts[0],
                fieldName: fieldParts[1]
            };
        }
    });
})();
