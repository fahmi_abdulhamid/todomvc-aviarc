/*global
YAHOO
*/

(function () {

    YAHOO.namespace("support.v1_0_0");
    var support = YAHOO.support.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    support.RemoveClass = function () {
        
    };

    YAHOO.lang.extend(support.RemoveClass, Toronto.framework.DefaultActionImpl, {

        run: function (state) {
            var widgetName = this.getAttribute("widget", state);
            var classNameAttribute = this.getAttribute("class", state);
            
            var theWidget = state.getExecutionState().getWidgetContext().findWidgetByID(widgetName);
            
            var classNames = classNameAttribute.split(" ");
            
            if (theWidget !== undefined){
                for (var i=0; i<classNames.length; i++) {
                    theWidget.removeClass(classNames[i]);
                }
            }
        }

    });

})();
