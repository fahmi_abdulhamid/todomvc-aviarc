/*global
YAHOO
*/

(function () {
    YAHOO.namespace("application.v1_0_0");
    var application = YAHOO.application.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.filterButtons = function() {
        application.filterButtons.superclass.constructor.apply(this, arguments);
        var me = this;
        this._sketchEvents.doLoop(function(eventName){
            me[eventName] = new Toronto.client.Event();
        });
    };

    YAHOO.lang.extend(application.filterButtons, Toronto.framework.DefaultTemplateWidgetImpl, {
        _sketchEvents: [
            "onClick",
            "onHover",
            "onMouseEnter",
            "onMouseLeave",
            "onMouseOver",
            "onMouseOut",
            "onMouseDown",
            "onMouseUp",
            "onWidgetShow",
            "onWidgetHide"
        ],
        // The 'startup' method may be deleted if it is not required, the method from DefaultWidgetImpl will be used
        // Removing the superclass.startup method call may prevent your widget from functioning
        startup: function (widgetContext) {
            application.filterButtons.superclass.startup.apply(this, arguments);
            // get xml attributes and delegate to the sketch widget so it behaves as a sketching widget
            var attrs = this.getXMLContext().getAttributes();
            var sketchWidget = this.getSketchWidget();
            var me = this;
            
            // need to call this as defered as we need to wait after the startup is complete on the sketch widget
            setTimeout(function(){
                for(var prop in attrs){
                   if(attrs.hasOwnProperty(prop)){
                       sketchWidget.attribute(prop, attrs[prop]);
                   }
                }
                
                me._sketchEvents.doLoop(function(eventName){
                    sketchWidget[eventName].bindHandler(function(ctx){
                        me[eventName].fireEvent({
                            widget:me,
                            action:ctx.action
                        });
                    });
                });
            });
        },
        SETHIDDEN: function(){
            this.getSketchWidget().SETHIDDEN.apply(this.getSketchWidget(), arguments);
        },
        
        attribute: function(){
            this.getSketchWidget().attribute.apply(this.getSketchWidget(), arguments);
        },
        
        getSketchWidget: function(){
            return this._widgetContext.getWidgetNode().getChildWidgets()[0];
        },
        getContainerElement: function(){
            return this.getSketchWidget().getContainerElement();
        }
    });
})();
