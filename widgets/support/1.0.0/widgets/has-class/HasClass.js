/*global
YAHOO
*/

(function() {

    YAHOO.namespace("support.v1_0_0");
    var support = YAHOO.support.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    support.HasClass = function() {

    };

    YAHOO.lang.extend(support.HasClass, Toronto.framework.DefaultActionImpl, {

        run: function(state) {

            var widgetName = this.getAttribute("widget", state);
            var className = this.getAttribute("class", state);

            var theWidget = state.getExecutionState().getWidgetContext().findWidgetByID(widgetName);
            
            var classExists = false;
            if (theWidget !== undefined) {
                var matchedClassNames = theWidget.getStyledElements()
                    // For each styled element get all of it's classes
                    .map(function(element) {
                        // The classList is a DOMTokenList. So, convert it into an array
                        var list = [];
                        list.push.apply(list, element.classList);
                        return list;
                    })
                    // Flatten the 2D array of classes (list of classes for each element)
                    .reduce(function(a, b) {
                        return a.concat(b);
                    })
                    // Get only classes which contain the target class name
                    .filter(function(aviarcClassName) {
                        return aviarcClassName.indexOf(className) !== -1;
                    });
                    
                classExists = matchedClassNames.length > 0;
            }
            
            
            state.getExecutionState().setReturnValue(classExists);
        }

    });

})();