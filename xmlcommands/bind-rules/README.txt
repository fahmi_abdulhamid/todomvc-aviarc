This is the 'bind-rules' cmd taken from:
    https://staff.coretech.co.nz/trac/changeset?new=66666%40motorsport%2Fmetadata%2Ftrunk%2Fxmlcommands%2Fapplication%2F1.0.0%2Fxmlcommands%2Fbind-rules%2Fsrc&old=63006%40motorsport%2Fmetadata%2Fbranches%2Flicence-and-user-management-RC2%2Fxmlcommands%2Fapplication%2F1.0.0%2Fxmlcommands%2Fbind-rules%2Fsrc

It was taken so that the rule set key is configurable. The namespace support has also been added.
