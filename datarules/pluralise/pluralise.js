(function (){

    YAHOO.namespace("todomvcaviarc.datarule");

    YAHOO.todomvcaviarc.datarule.PluraliseDataRule = function(context){
        this.field = context['field'];
        this.referenceField = context['reference-field'];
    };

    YAHOO.todomvcaviarc.datarule.PluraliseDataRule.prototype = {
        initialize: function(context) {
            this._context = context.getUpdateContext();
            this.dataset = context.getDataset();
            
            this.singularText = this.dataset.getCurrentRowField(this.field);
            this.dataset.bindOnCurrentRowFieldChangedHandler(this.referenceField, this.formatValue, this);
        },
        
        formatValue: function(context) {
            var referenceValue = parseInt(context.newValue, 10);
            var result = this.singularText;
            
            if(referenceValue !== 0 && referenceValue !== 1) {
                var firstSingularWord = this.singularText.split(/\s/)[0].trim();
                result = this.singularText.replace(firstSingularWord, this._makePlural(firstSingularWord), 1);
            }
            
            this.dataset.setCurrentRowField(this.field, result);
        },
        
        _makePlural: function(singularWord) {
            return singularWord + 's'; // Good enough for now
        }
    };

})();
